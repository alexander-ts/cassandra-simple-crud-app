package app.controller;

import app.dao.UserPersistence;
import app.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class UserController {

    @Autowired
    private UserPersistence userPersistence;

    @RequestMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }

    @RequestMapping("/users")
    public List<User> getAllUsers() {
        List<User> result = new ArrayList<>();
        List<Optional<User>> optList = userPersistence.queryAllUsers();
        optList.stream().forEach(optUser -> result.add(optUser.get()));
        return result;
    }

    @RequestMapping("/users/{id}")
    public User getUser(@PathVariable Integer id) {
        Optional<User> optUser = userPersistence.findOne(id);
        if(optUser.isPresent())
            return optUser.get();
        else
            return null;
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public String createUser(@RequestBody User user) {
        userPersistence.createUser(user.getId(), user.getFname(), user.getLname(), user.getEmail());
        return "User persisted";
    }

    @RequestMapping(value = "/users", method = RequestMethod.PUT)
    public String updateUser(@RequestBody User user) {
        userPersistence.updateUser(user.getId(), user.getFname(), user.getLname(), user.getEmail());
        return "User persisted";
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
    public String deleteUser(@PathVariable Integer id) {
        userPersistence.deleteUser(id);
        return "User deleted";
    }
}