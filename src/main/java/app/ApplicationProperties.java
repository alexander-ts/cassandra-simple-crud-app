package app;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:application.properties")
@ConfigurationProperties(prefix = "datasource")
public class ApplicationProperties {

    private String url;
    private Integer port;

    public String getUrl() {
        return url;
    }

    public void setUrl(String nUrl) {
        this.url = nUrl;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer nPort) {
        this.port = nPort;
    }
}
