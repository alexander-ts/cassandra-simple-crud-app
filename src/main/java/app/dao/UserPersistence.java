package app.dao;

import app.ApplicationProperties;
import app.CassandraConnector;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import app.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static java.lang.System.out;

@Service
@PropertySource("classpath:application.properties")
public class UserPersistence {

    private CassandraConnector client;
    private ApplicationProperties applicationProperties;

    @Autowired
    public UserPersistence(ApplicationProperties applicationProperties, CassandraConnector client) {
        this.client = client;
        this.applicationProperties = applicationProperties;
        out.println("Connecting to IP Address " + applicationProperties.getUrl() + ":" + applicationProperties.getPort() + "...");
        client.connect(applicationProperties.getUrl(), applicationProperties.getPort());
    }
    /**
     * Close my underlying Cassandra connection.
     */
    private void close() {
        client.close();
    }

    /**
     * Persist provided user information.
     *
     * @param id ID of user to be persisted.
     * @param fname First Name of user to be persisted.
     * @param lname Last Name of user to be persisted.
     * @param email Email of user to be persisted.
     */
    public void persistUser(final Integer id, final String fname, final String lname, final String email) {
        client.getSession().execute(
                "INSERT INTO cassandra_test.user (userid, user_fname, user_lname, user_email) VALUES (?, ?, ?, ?)",
                id, fname, lname, email);
    }

    /**
     * Create user with provided information.
     *
     * @param id ID of user to be persisted.
     * @param fname First Name of user to be persisted.
     * @param lname Last Name of user to be persisted.
     * @param email Email of user to be persisted.
     */
    public void createUser(final Integer id, final String fname, final String lname, final String email) {
        client.getSession().execute(
                "INSERT INTO cassandra_test.user (userid, user_fname, user_lname, user_email) VALUES (?, ?, ?, ?) IF NOT EXISTS",
                id, fname, lname, email);
    }

    /**
     * Update user with provided information.
     *
     * @param id ID of user.
     * @param fname First Name of user to be updated.
     * @param lname Last Name of user to be updated.
     * @param email Email of user to be updated.
     */
    public void updateUser(final Integer id, final String fname, final String lname, final String email) {
        client.getSession().execute(
                "UPDATE cassandra_test.user SET user_fname = ?, user_lname = ?, user_email = ? WHERE userid = ? IF EXISTS",
                fname, lname, email, id);
    }

    /**
     * Returns user matching provided id.
     *
     * @param userid ID of desired user.
     * @return Desired user if match is found; Optional.empty() if no match is found.
     */
    public Optional<User> findOne(final int userid) {
        final ResultSet userResults = client.getSession().execute(
                "SELECT * from cassandra_test.user WHERE userid = ?", userid);
        final Row userRow = userResults.one();
        final Optional<User> user =
                userRow != null
                        ? Optional.of(new User(
                        userRow.getInt("userid"),
                        userRow.getString("user_fname"),
                        userRow.getString("user_lname"),
                        userRow.getString("user_email")))
                        : Optional.empty();
        return user;
    }

    /**
     * Returns all users.
     *
     * @return Desired user if match is found; Optional.empty() if no match is found.
     */
    public List<Optional<User>> queryAllUsers() {
        final ResultSet userResults = client.getSession().execute(
                "SELECT * from cassandra_test.user");
        List<Optional<User>> users = new ArrayList<>();
        final List<Row> userRows = userResults.all();
        for(Row userRow : userRows) {
            final Optional<User> user =
                    userRow != null
                            ? Optional.of(new User(
                            userRow.getInt("userid"),
                            userRow.getString("user_fname"),
                            userRow.getString("user_lname"),
                            userRow.getString("user_email")))
                            : Optional.empty();
            users.add(user);
        }

        return users;
    }

    /**
     * Deletes the user with the provided id.
     *
     * @param id ID of user to be deleted.
     */
    public void deleteUser(final Integer id) {
        final String deleteString = "DELETE FROM cassandra_test.user WHERE userid = ?";
        client.getSession().execute(deleteString, id);
    }
}
